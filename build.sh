#!/bin/bash

export BIKER_BASE_PATH=$(pwd)
export BIKER_APP_PATH=$(pwd)/app
export BIKER_BLOCKCHAIN_PATH=$(pwd)/blockchain
export BIKER_BIN_PATH=$BIKER_BASE_PATH/bin
export BIKER_CONFIG_PATH=$BIKER_BLOCKCHAIN_PATH/config
export BIKER_CONFIGDATA_PATH=$BIKER_CONFIG_PATH/data
export FABRIC_CFG_PATH=$BIKER_CONFIG_PATH

$BIKER_BLOCKCHAIN_PATH/generate-certs.sh
if [ "$1" = "BUILD" ]
then
    $BIKER_BLOCKCHAIN_PATH/generate-images.sh
fi
docker-compose up -d
docker-compose logs -f
