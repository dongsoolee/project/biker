#!/bin/sh

APP_PATH=$BIKER_APP_PATH
BIN_PATH=$BIKER_BIN_PATH
CONFIGDATA_PATH=$BIKER_CONFIGDATA_PATH
CHANNEL_NAME=$1

echo
echo "##########################################################"
echo "#########  Generating Orderer Genesis block ##############"
echo "##########################################################"
$BIN_PATH/configtxgen -profile BikerOrgsGenesis -outputBlock $CONFIGDATA_PATH/genesis.block

echo
echo "#################################################################"
echo "### Generating channel configuration transaction 'channel.tx' ###"
echo "#################################################################"
$BIN_PATH/configtxgen -profile BikerOrgsChannel -outputCreateChannelTx $CONFIGDATA_PATH/channel.tx -channelID $CHANNEL_NAME
rm $APP_PATH/blockchain/channel.tx
cp $CONFIGDATA_PATH/channel.tx $APP_PATH/blockchain/channel.tx

echo
echo "#################################################################"
echo "####### Generating anchor peer update for UserOrg ##########"
echo "#################################################################"
$BIN_PATH/configtxgen -profile BikerOrgsChannel -outputAnchorPeersUpdate $CONFIGDATA_PATH/UserOrgMSPAnchors.tx -channelID $CHANNEL_NAME -asOrg UserOrgMSP

echo
echo "#################################################################"
echo "#######    Generating anchor peer update for BikeOrg   ##########"
echo "#################################################################"
$BIN_PATH/configtxgen -profile BikerOrgsChannel -outputAnchorPeersUpdate $CONFIGDATA_PATH/BikeOrgMSPAnchors.tx -channelID $CHANNEL_NAME -asOrg BikeOrgMSP

echo
echo "##################################################################"
echo "#######   Generating anchor peer update for CompanyOrg   ##########"
echo "##################################################################"
$BIN_PATH/configtxgen -profile BikerOrgsChannel -outputAnchorPeersUpdate $CONFIGDATA_PATH/CompanyOrgMSPAnchors.tx -channelID $CHANNEL_NAME -asOrg CompanyOrgMSP

echo
echo "##################################################################"
echo "####### Generating anchor peer update for RepairshopOrg ##########"
echo "##################################################################"
$BIN_PATH/configtxgen -profile BikerOrgsChannel -outputAnchorPeersUpdate $CONFIGDATA_PATH/RepairshopOrgMSPAnchors.tx -channelID $CHANNEL_NAME -asOrg RepairshopOrgMSP
