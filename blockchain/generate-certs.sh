#!/bin/sh
set -e

echo
echo "#################################################################"
echo "#######        Generating cryptographic material       ##########"
echo "#################################################################"

BASE_PATH=$BIKER_BLOCKCHAIN_PATH
BIN_PATH=$BIKER_BIN_PATH
CONFIG_PATH=$BIKER_CONFIG_PATH
CONFIGDATA_PATH=$BIKER_CONFIGDATA_PATH
ORDERER_PATH=$BASE_PATH/orderer
PEERS_PATH=$BASE_PATH/peers

CONFIG_ORDERERS=$CONFIGDATA_PATH/ordererOrganizations
CONFIG_PEERS=$CONFIGDATA_PATH/peerOrganizations

rm -rf $CONFIGDATA_PATH
$BIN_PATH/cryptogen generate --config=$CONFIG_PATH/config.yml --output=$CONFIGDATA_PATH

$BASE_PATH/generate-cftx.sh default

echo
echo "######################################################################"
echo "#######   Copying certificate files in blockchain directory ##########"
echo "######################################################################"

rm -rf $ORDERER_PATH/crypto
rm -rf $PEERS_PATH/{userPeer,bikePeer,companyPeer,repairshopPeer}/crypto
mkdir $ORDERER_PATH/crypto
mkdir $PEERS_PATH/{userPeer,bikePeer,companyPeer,repairshopPeer}/crypto

cp -r $CONFIG_ORDERERS/orderer-org/orderers/orderer0/{msp,tls} $ORDERER_PATH/crypto
cp -r $CONFIG_PEERS/user-org/peers/user-peer/{msp,tls} $PEERS_PATH/userPeer/crypto
cp -r $CONFIG_PEERS/bike-org/peers/bike-peer/{msp,tls} $PEERS_PATH/bikePeer/crypto
cp -r $CONFIG_PEERS/company-org/peers/company-peer/{msp,tls} $PEERS_PATH/companyPeer/crypto
cp -r $CONFIG_PEERS/repairshop-org/peers/repairshop-peer/{msp,tls} $PEERS_PATH/repairshopPeer/crypto
cp $CONFIGDATA_PATH/genesis.block $ORDERER_PATH/crypto/

USER_CA_PATH=$PEERS_PATH/userCA
BIKE_CA_PATH=$PEERS_PATH/bikeCA
COMPANY_CA_PATH=$PEERS_PATH/companyCA
REPAIRSHOP_CA_PATH=$PEERS_PATH/repairshopCA

rm -rf {$USER_CA_PATH,$BIKE_CA_PATH,$COMPANY_CA_PATH,$REPAIRSHOP_CA_PATH}/{ca,tls}
mkdir -p {$USER_CA_PATH,$BIKE_CA_PATH,$COMPANY_CA_PATH,$REPAIRSHOP_CA_PATH}/{ca,tls}
cp $CONFIG_PEERS/user-org/ca/* $USER_CA_PATH/ca
cp $CONFIG_PEERS/user-org/tlsca/* $USER_CA_PATH/tls
mv $USER_CA_PATH/ca/*_sk $USER_CA_PATH/ca/key.pem
mv $USER_CA_PATH/ca/*-cert.pem $USER_CA_PATH/ca/cert.pem
mv $USER_CA_PATH/tls/*_sk $USER_CA_PATH/tls/key.pem
mv $USER_CA_PATH/tls/*-cert.pem $USER_CA_PATH/tls/cert.pem

cp $CONFIG_PEERS/bike-org/ca/* $BIKE_CA_PATH/ca
cp $CONFIG_PEERS/bike-org/tlsca/* $BIKE_CA_PATH/tls
mv $BIKE_CA_PATH/ca/*_sk $BIKE_CA_PATH/ca/key.pem
mv $BIKE_CA_PATH/ca/*-cert.pem $BIKE_CA_PATH/ca/cert.pem
mv $BIKE_CA_PATH/tls/*_sk $BIKE_CA_PATH/tls/key.pem
mv $BIKE_CA_PATH/tls/*-cert.pem $BIKE_CA_PATH/tls/cert.pem

cp $CONFIG_PEERS/company-org/ca/* $COMPANY_CA_PATH/ca
cp $CONFIG_PEERS/company-org/tlsca/* $COMPANY_CA_PATH/tls
mv $COMPANY_CA_PATH/ca/*_sk $COMPANY_CA_PATH/ca/key.pem
mv $COMPANY_CA_PATH/ca/*-cert.pem $COMPANY_CA_PATH/ca/cert.pem
mv $COMPANY_CA_PATH/tls/*_sk $COMPANY_CA_PATH/tls/key.pem
mv $COMPANY_CA_PATH/tls/*-cert.pem $COMPANY_CA_PATH/tls/cert.pem

cp $CONFIG_PEERS/repairshop-org/ca/* $REPAIRSHOP_CA_PATH/ca
cp $CONFIG_PEERS/repairshop-org/tlsca/* $REPAIRSHOP_CA_PATH/tls
mv $REPAIRSHOP_CA_PATH/ca/*_sk $REPAIRSHOP_CA_PATH/ca/key.pem
mv $REPAIRSHOP_CA_PATH/ca/*-cert.pem $REPAIRSHOP_CA_PATH/ca/cert.pem
mv $REPAIRSHOP_CA_PATH/tls/*_sk $REPAIRSHOP_CA_PATH/tls/key.pem
mv $REPAIRSHOP_CA_PATH/tls/*-cert.pem $REPAIRSHOP_CA_PATH/tls/cert.pem

echo
echo "###############################################################"
echo "#######   Copying certificate files in app directory ##########"
echo "###############################################################"

APPCERTS_PATH=$BIKER_APP_PATH/blockchain/certs
rm -rf $APPCERTS_PATH
mkdir -p $APPCERTS_PATH
cp $ORDERER_PATH/crypto/tls/ca.crt $APPCERTS_PATH/ordererOrg.pem
cp $PEERS_PATH/userPeer/crypto/tls/ca.crt $APPCERTS_PATH/userOrg.pem
cp $PEERS_PATH/bikePeer/crypto/tls/ca.crt $APPCERTS_PATH/bikeOrg.pem
cp $PEERS_PATH/companyPeer/crypto/tls/ca.crt $APPCERTS_PATH/companyOrg.pem
cp $PEERS_PATH/repairshopPeer/crypto/tls/ca.crt $APPCERTS_PATH/repairshopOrg.pem
cp $CONFIG_PEERS/user-org/users/Admin@user-org/msp/keystore/* $APPCERTS_PATH/Admin@user-org-key.pem
cp $CONFIG_PEERS/user-org/users/Admin@user-org/msp/signcerts/* $APPCERTS_PATH/
cp $CONFIG_PEERS/repairshop-org/users/Admin@repairshop-org/msp/keystore/* $APPCERTS_PATH/Admin@repairshop-org-key.pem
cp $CONFIG_PEERS/repairshop-org/users/Admin@repairshop-org/msp/signcerts/* $APPCERTS_PATH/
cp $CONFIG_PEERS/bike-org/users/Admin@bike-org/msp/keystore/* $APPCERTS_PATH/Admin@bike-org-key.pem
cp $CONFIG_PEERS/bike-org/users/Admin@bike-org/msp/signcerts/* $APPCERTS_PATH/
cp $CONFIG_PEERS/company-org/users/Admin@company-org/msp/keystore/* $APPCERTS_PATH/Admin@company-org-key.pem
cp $CONFIG_PEERS/company-org/users/Admin@company-org/msp/signcerts/* $APPCERTS_PATH/

echo
echo "################################################################"
echo "#######        Completed cryptographic material       ##########"
echo "################################################################"
