#!/bin/sh
set -e

echo
echo "############################################################"
echo "#######        Pulling latest fabric images       ##########"
echo "############################################################"

for IMAGES in peer orderer ccenv; do
    echo "==> FABRIC IMAGE: $IMAGES"
    echo
    docker pull hyperledger/fabric-$IMAGES:latest
    docker tag hyperledger/fabric-$IMAGES:latest hyperledger/fabric-$IMAGES
done

echo
echo "############################################################"
echo "#######        Pulling latest fabric images       ##########"
echo "############################################################"
echo "==> FABRIC CA IMAGE"
echo
docker pull hyperledger/fabric-ca:latest
docker tag hyperledger/fabric-ca:latest hyperledger/fabric-ca

echo '############################################################'
echo '#                 BUILDING CONTAINER IMAGES                #'
echo '############################################################'
docker build -t orderer:latest $BIKER_BLOCKCHAIN_PATH/orderer/
docker build -t user-peer:latest $BIKER_BLOCKCHAIN_PATH/peers/userPeer/
docker build -t bike-peer:latest $BIKER_BLOCKCHAIN_PATH/peers/bikePeer/
docker build -t company-peer:latest $BIKER_BLOCKCHAIN_PATH/peers/companyPeer/
docker build -t repairshop-peer:latest $BIKER_BLOCKCHAIN_PATH/peers/repairshopPeer/
docker build -t user-ca:latest $BIKER_BLOCKCHAIN_PATH/peers/userCA/
docker build -t bike-ca:latest $BIKER_BLOCKCHAIN_PATH/peers/bikeCA/
docker build -t company-ca:latest $BIKER_BLOCKCHAIN_PATH/peers/companyCA/
docker build -t repairshop-ca:latest $BIKER_BLOCKCHAIN_PATH/peers/repairshopCA/
#docker build -t app:latest $BIKER_APP_PATH
