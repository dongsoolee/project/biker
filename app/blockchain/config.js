const { readFileSync } = require('fs');
const { resolve } = require('path');

const basePath = resolve(__dirname, './certs');
const readCryptoFile = filename => readFileSync(resolve(basePath, filename)).toString();
const config = {
  isCloud: false,
  isUbuntu: false,
  channelName: 'default',
  channelConfig: readFileSync(resolve(__dirname, './channel.tx')),
  chaincodeId: 'bcins',
  chaincodeVersion: 'v2',
  chaincodePath: 'bcins',
  orderer0: {
    hostname: 'orderer0',
    url: 'grpcs://orderer0:7050',
    pem: readCryptoFile('ordererOrg.pem')
  },
  userOrg: {
    peer: {
      hostname: 'user-peer',
      url: 'grpcs://user-peer:7051',
      eventHubUrl: 'grpcs://user-peer:7053',
      pem: readCryptoFile('userOrg.pem')
    },
    ca: {
      hostname: 'user-ca',
      url: 'https://user-ca:7054',
      mspId: 'UserOrgMSP'
    },
    admin: {
      key: readCryptoFile('Admin@user-org-key.pem'),
      cert: readCryptoFile('Admin@user-org-cert.pem')
    }
  },
  bikeOrg: {
    peer: {
      hostname: 'bike-peer',
      url: 'grpcs://bike-peer:7051',
      eventHubUrl: 'grpcs://bike-peer:7053',
      pem: readCryptoFile('bikeOrg.pem')
    },
    ca: {
      hostname: 'bike-ca',
      url: 'https://bike-ca:7054',
      mspId: 'BikeOrgMSP'
    },
    admin: {
      key: readCryptoFile('Admin@bike-org-key.pem'),
      cert: readCryptoFile('Admin@bike-org-cert.pem')
    }
  },
  companyOrg: {
    peer: {
      hostname: 'company-peer',
      url: 'grpcs://company-peer:7051',
      eventHubUrl: 'grpcs://company-peer:7053',
      pem: readCryptoFile('companyOrg.pem')
    },
    ca: {
      hostname: 'company-ca',
      url: 'https://company-ca:7054',
      mspId: 'CompanyOrgMSP'
    },
    admin: {
      key: readCryptoFile('Admin@company-org-key.pem'),
      cert: readCryptoFile('Admin@company-org-cert.pem')
    }
  },
  repairshopOrg: {
    peer: {
      hostname: 'repairshop-peer',
      url: 'grpcs://repairshop-peer:7051',
      eventHubUrl: 'grpcs://repairshop-peer:7053',
      pem: readCryptoFile('repairshopOrg.pem'),
    },
    ca: {
      hostname: 'repairshop-ca',
      url: 'https://repairshop-ca:7054',
      mspId: 'RepairshopOrgMSP'
    },
    admin: {
      key: readCryptoFile('Admin@repairshop-org-key.pem'),
      cert: readCryptoFile('Admin@repairshop-org-cert.pem')
    }
  }
};

if (process.env.NODE_ENV === 'local') {
  config.orderer0.url = 'grpcs://localhost:7050';

  config.userOrg.peer.url = 'grpcs://localhost:8001';
  config.bikeOrg.peer.url = 'grpcs://localhost:8002';
  config.companyOrg.peer.url = 'grpcs://localhost:8003';
  config.repairshopOrg.peer.url = 'grpcs://localhost:8004';

  config.userOrg.peer.eventHubUrl = 'grpcs://localhost:8101';
  config.bikeOrg.peer.eventHubUrl = 'grpcs://localhost:8102';
  config.companyOrg.peer.eventHubUrl = 'grpcs://localhost:8103';
  config.repairshopOrg.peer.eventHubUrl = 'grpcs://localhost:8104';

  config.userOrg.ca.url = 'https://localhost:7001';
  config.bikeOrg.ca.url = 'https://localhost:7002';
  config.companyOrg.ca.url = 'https://localhost:7003';
  config.repairshopOrg.ca.url = 'https://localhost:7004';
}

const DEFAULT_CONTRACT_TYPES = [
  {
    uuid: '63ef076a-33a1-41d2-a9bc-2777505b014f',
    shopType: 'B',
    formulaPerDay: 'price * 0.01 + 0.05',
    maxSumInsured: 4300.00,
    theftInsured: true,
    description: 'Contract for Mountain Bikers',
    conditions: 'Contract Terms here',
    minDurationDays: 1,
    maxDurationDays: 7,
    active: true
  },
  {
    uuid: '1d640cf7-9808-4c78-b7f0-55aaad02e9e5',
    shopType: 'B',
    formulaPerDay: 'price * 0.02',
    maxSumInsured: 3500.00,
    theftInsured: false,
    description: 'Insure Your Bike',
    conditions: 'Simple contract terms.',
    minDurationDays: 3,
    maxDurationDays: 10,
    active: true
  },
  {
    uuid: '17210a72-f505-42bf-a238-65c8898477e1',
    shopType: 'P',
    formulaPerDay: 'price * 0.001 + 5.00',
    maxSumInsured: 1500.00,
    theftInsured: true,
    description: 'Phone Insurance Contract',
    conditions: 'Exemplary contract terms here.',
    minDurationDays: 5,
    maxDurationDays: 10,
    active: true
  },
  {
    uuid: '17d773dc-2624-4c22-a478-87544dd0a17f',
    shopType: 'P',
    formulaPerDay: 'price * 0.005 + 10.00',
    maxSumInsured: 2500.00,
    theftInsured: true,
    description: 'Premium SmartPhone Insurance',
    conditions: 'Only for premium phone owners.',
    minDurationDays: 10,
    maxDurationDays: 20,
    active: true
  },
  {
    uuid: 'd804f730-8c77-4583-9247-ec9e753643db',
    shopType: 'S',
    formulaPerDay: '25.00',
    maxSumInsured: 5000.00,
    theftInsured: false,
    description: 'Short-Term Ski Insurance',
    conditions: 'Simple contract terms here.',
    minDurationDays: 3,
    maxDurationDays: 25,
    active: true
  },
  {
    uuid: 'dcee27d7-bf3c-4995-a272-8a306a35e51f',
    shopType: 'S',
    formulaPerDay: 'price * 0.001 + 10.00',
    maxSumInsured: 3000.00,
    theftInsured: true,
    description: 'Insure Ur Ski',
    conditions: 'Just do it.',
    minDurationDays: 1,
    maxDurationDays: 15,
    active: true
  },
  {
    uuid: 'c06f95d6-9b90-4d24-b8cb-f347d1b33ddf',
    shopType: 'BPS',
    formulaPerDay: '50',
    maxSumInsured: 3000.00,
    theftInsured: false,
    description: 'Universal Insurance Contract',
    conditions: 'Universal Contract Terms here. For all types of goods.',
    minDurationDays: 1,
    maxDurationDays: 10,
    active: true
  }
];

module.exports = config;