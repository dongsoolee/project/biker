const config = require('./config');
const OrganizationClient = require('./client');

const userClient = new OrganizationClient(
    config.channelName,
    config.orderer0,
    config.userOrg.peer,
    config.userOrg.ca,
    config.userOrg.admin
);
console.log({
  a: config.channelName,
  b:config.orderer0,
  c:config.userOrg.peer,
  d:config.userOrg.ca,
  e:config.userOrg.admin
});
const bikeClient = new OrganizationClient(
    config.channelName,
    config.orderer0,
    config.bikeOrg.peer,
    config.bikeOrg.ca,
    config.bikeOrg.admin
);
const companyClient = new OrganizationClient(
    config.channelName,
    config.orderer0,
    config.companyOrg.peer,
    config.companyOrg.ca,
    config.companyOrg.admin
);
const repairshopClient = new OrganizationClient(
    config.channelName,
    config.orderer0,
    config.repairshopOrg.peer,
    config.repairshopOrg.ca,
    config.repairshopOrg.admin
);

const CLIENTS = [userClient];

async function execClients(funcName) {
  return Promise.all(CLIENTS.map(async (client) => {
    return client[funcName]();
  }));
}

(async () => {
  // Login
  try {
    console.log("Login");
    await execClients('login');
    console.log("Login Complete");
  } catch (e) {
    console.log('Fatal error logging into blockchain organization clients!');
    console.log(e);
    process.exit(-1);
  }

  // Bootstrap blockchain network
  try {
    await execClients('getOrgAdmin');
    if (!(await userClient.checkChannelMembership())) {
      console.log('Default channel not found, attempting creation...');
      const createChannelResponse = await userClient.createChannel(config.channelConfig);
      if (createChannelResponse.status === 'SUCCESS') {
        console.log('Successfully created a new default channel.');
        console.log('Joining peers to the default channel.');
        await execClients('joinChannel');
        // Wait for 10s for the peers to join the newly created channel
        await new Promise(resolve => {
          setTimeout(resolve, 10000);
        });
      }
    }
  } catch (e) {
    console.log('Fatal error bootstrapping the blockchain network!');
    console.log(e);
    process.exit(-1);
  }
  //
  // // Register block events
  // try {
  //   console.log('Connecting and Registering Block Events');
  //   userClient.connectAndRegisterBlockEvent();
  //   bikeClient.connectAndRegisterBlockEvent();
  //   companyClient.connectAndRegisterBlockEvent();
  //   repairshopClient.connectAndRegisterBlockEvent();
  // } catch (e) {
  //   console.log('Fatal error register block event!');
  //   console.log(e);
  //   process.exit(-1);
  // }
  //
  // // Initialize network
  // try {
  //   await Promise.all([
  //     userClient.initialize(),
  //     bikeClient.initialize(),
  //     companyClient.initialize(),
  //     repairshopClient.initialize()
  //   ]);
  // } catch (e) {
  //   console.log('Fatal error initializing blockchain organization clients!');
  //   console.log(e);
  //   process.exit(-1);
  // }
  //
  // // Install chaincode on all peers
  // let installedOnInsuranceOrg, installedOnShopOrg, installedOnRepairShopOrg,
  //     installedOnPoliceOrg;
  // try {
  //   await getAdminOrgs();
  //   installedOnInsuranceOrg = await userClient.checkInstalled(
  //       config.chaincodeId, config.chaincodeVersion, config.chaincodePath);
  //   installedOnShopOrg = await bikeClient.checkInstalled(
  //       config.chaincodeId, config.chaincodeVersion, config.chaincodePath);
  //   installedOnRepairShopOrg = await companyClient.checkInstalled(
  //       config.chaincodeId, config.chaincodeVersion, config.chaincodePath);
  //   installedOnPoliceOrg = await repairshopClient.checkInstalled(
  //       config.chaincodeId, config.chaincodeVersion, config.chaincodePath);
  // } catch (e) {
  //   console.log('Fatal error getting installation status of the chaincode!');
  //   console.log(e);
  //   process.exit(-1);
  // }
  //
  // if (!(installedOnInsuranceOrg && installedOnShopOrg &&
  //     installedOnRepairShopOrg && installedOnPoliceOrg)) {
  //   console.log('Chaincode is not installed, attempting installation...');
  //
  //   // Pull chaincode environment base image
  //   try {
  //     await getAdminOrgs();
  //     const socketPath = process.env.DOCKER_SOCKET_PATH ||
  //         (process.platform === 'win32' ? '//./pipe/docker_engine' : '/var/run/docker.sock');
  //     const ccenvImage = process.env.DOCKER_CCENV_IMAGE ||
  //         'hyperledger/fabric-ccenv:latest';
  //     const listOpts = { socketPath, method: 'GET', path: '/images/json' };
  //     const pullOpts = {
  //       socketPath, method: 'POST',
  //       path: url.format({ pathname: '/images/create', query: { fromImage: ccenvImage } })
  //     };
  //
  //     const images = await new Promise((resolve, reject) => {
  //       const req = http.request(listOpts, (response) => {
  //         let data = '';
  //         response.setEncoding('utf-8');
  //         response.on('data', chunk => { data += chunk; });
  //         response.on('end', () => { resolve(JSON.parse(data)); });
  //       });
  //       req.on('error', reject); req.end();
  //     });
  //
  //     const imageExists = images.some(
  //         i => i.RepoTags && i.RepoTags.some(tag => tag === ccenvImage));
  //     if (!imageExists) {
  //       console.log(
  //           'Base container image not present, pulling from Docker Hub...');
  //       await new Promise((resolve, reject) => {
  //         const req = http.request(pullOpts, (response) => {
  //           response.on('data', () => { });
  //           response.on('end', () => { resolve(); });
  //         });
  //         req.on('error', reject); req.end();
  //       });
  //       console.log('Base container image downloaded.');
  //     } else {
  //       console.log('Base container image present.');
  //     }
  //   } catch (e) {
  //     console.log('Fatal error pulling docker images.');
  //     console.log(e);
  //     process.exit(-1);
  //   }
  //
  //   // Install chaincode
  //   const installationPromises = [
  //     userClient.install(
  //         config.chaincodeId, config.chaincodeVersion, config.chaincodePath),
  //     bikeClient.install(
  //         config.chaincodeId, config.chaincodeVersion, config.chaincodePath),
  //     companyClient.install(
  //         config.chaincodeId, config.chaincodeVersion, config.chaincodePath),
  //     repairshopClient.install(
  //         config.chaincodeId, config.chaincodeVersion, config.chaincodePath)
  //   ];
  //   try {
  //     await Promise.all(installationPromises);
  //     await new Promise(resolve => {   setTimeout(resolve, 10000); });
  //     console.log('Successfully installed chaincode on the default channel.');
  //   } catch (e) {
  //     console.log('Fatal error installing chaincode on the default channel!');
  //     console.log(e);
  //     process.exit(-1);
  //   }
  //
  //   // Instantiate chaincode on all peers
  //   // Instantiating the chaincode on a single peer should be enough (for now)
  //   try {
  //     // Initial contract types
  //     await userClient.instantiate(config.chaincodeId,
  //         config.chaincodeVersion, DEFAULT_CONTRACT_TYPES);
  //     console.log('Successfully instantiated chaincode on all peers.');
  //     setStatus('ready');
  //   } catch (e) {
  //     console.log('Fatal error instantiating chaincode on some(all) peers!');
  //     console.log(e);
  //     process.exit(-1);
  //   }
  // } else {
  //   console.log('Chaincode already installed on the blockchain network.');
  //   setStatus('ready');
  // }
})();

module.exports = {
  userClient,
  bikeClient,
  companyClient,
  repairshopClient,
};